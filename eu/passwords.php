<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Pasahitzak gutxienez 6 karaktere izan behar  ditu eta konfirmazioaren berdina izan behar du.',
    'reset' => 'Zure pasahitza berrezarri da!',
    'sent' => 'Pasahitzaren oroigarria bidalita!',
    'token' => 'Pasahitza berrezartzeko token honek ez du balio.',
    'user' => 'Ez da helbide hori daukan erabiltzailerik topatu.',

];
