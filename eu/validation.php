<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute eremua onartua izan behar da.',
    'active_url'           => ':attribute eremua ez da baliozko url bat.',
    'after'                => ':attribute eremua :date ondorengo data bat izan behar da.',
    'after_or_equal'       => ':attribute eremua :date ondorengo edo berdina den data bat izan behar da.',
    'alpha'                => ':attribute eremuak hizkiak bakarrik izan ditzake.',
    'alpha_dash'           => ':attribute eremuak hizkiak, zenbakiak eta gidoiak soilik izan ditzake (a-z, 0-9, -_).',
    'alpha_num'            => ':attribute eremuak hizkiak eta zenbakiak bakarrik izan ditzake.',
    'array'                => ':attribute eremua array bat izan behar da.',
    'before'               => ':attribute eremua :date aurreko data bat izan behar da.',
    'before_or_equal'      => ':attribute eremua :date aurreko edo berdina den data bat izan behar da.',
    'between'              => [
        'numeric' => ':attribute eremua :min eta :max arteko balio bat izan behar da.',
        'file'    => ':attribute fitxategiak :min eta :max kilobyte arteko pisua izan behar du.',
        'string'  => ':attribute eremuak :min eta :max arteko karaktere kopurua izan behar du.',
        'array'   => ':attribute eremuak :min eta :max arteko elementu kopurua izan behar du.',
    ],
    'boolean'              => ':attribute eremua egia edo gezurra izan behar da.',
    'confirmed'            => ':attribute eremuaren konfirmaziorako eremuak ez du koinziditzen.',
    'date'                 => ':attribute eremuak ez du baliozko data batekin koinziditzen.',
    'date_format'          => ':attribute eremua ez dator bat :format data formatuarekin.',
    'different'            => ':attribute eta :other eremuak ezberdinak izan behar dira.',
    'digits'               => ':attribute eremua :digits digituko zenbakia izan behar da.',
    'digits_between'       => ':attribute eremua :min eta :max digitu arteko zenbakia izan behar da.',
    'dimensions'           => ':attribute eremuak baliogabeko dimensioak ditu.',
    'distinct'             => ':attribute eremuak errepikatutako balioa dauka.',
    'email'                => ':attribute eremua ez dator bat baliozko e-posta helbide batekin.',
    'file'                 => ':attribute eremua fitxategi bat izan behar da.',
    'filled'               => ':attribute eremua nahitaezkoa da.',
    'exists'               => ':attribute eremua ez da existitzen.',
    'image'                => ':attribute eremua irudi bat izan behar da.',
    'in'                   => ':attribute eremua balio hauetako baten berdina izan behar da :values.',
    'in_array'             => ':attribute eremua ez da :other-en existitzen.',
    'integer'              => ':attribute eremua zenbaki oso bat izan behar da.',
    'ip'                   => ':attribute eremua baliozko IP helbide bat izan behar da.',
    'ipv4'                 => ':attribute eremua baliozko IPv4 helbide bat izan behar da.',
    'ipv6'                 => ':attribute eremua baliozko IPv6 helbide bat izan behar da.',
    'json'                 => ':attribute eremua baliozko JSON testu kate bat izan behar da.',
    'max'                  => [
        'numeric' => ':attribute eremuaren gehienezko balioa :max izan behar da.',
        'file'    => ':attribute fitxategiaren gehienezko pisua :max kilobytekoa izan behar da.',
        'string'  => ':attribute eremuak gehienez :max karaktere izan behar ditu.',
        'array'   => ':attribute eremuak gehienez :max elementu izan behar ditu.',
    ],
    'mimes'                => ':attribute eremua :values motako fitxategi bat izan behar da.',
    'mimetypes'            => ':attribute eremua :values motako fitxategi bat izan behar da.',
    'min'                  => [
        'numeric' => ':attribute eremuaren gutxieneko balioa :min izan behar da.',
        'file'    => ':attribute eremuaren gutxienezko pisua :min kilobytekoa izan behar da.',
        'string'  => ':attribute eremuak gutxienez :min karaktere izan behar ditu.',
        'array'   => ':attribute eremuak ezin du :min elementu baino gehiago izan.',
    ],
    'not_in'               => 'Aukeratutako :attribute eremua baliogabea da.',
    'numeric'              => ':attribute eremua zenbaki bat izan behar da.',
    'present'              => ':attribute eremuak egon behar du.',
    'regex'                => ':attribute eremuaren formatua baliogabea da.',
    'required'             => ':attribute eremua nahitaezkoa da.',
    'required_if'          => ':attribute eremua nahitaezkoa da :other eremua :value denean.',
    'required_unless'      => ':attribute eremua nahitaezkoa da :other eremua :values-en ez badago.',
    'required_with'        => ':attribute eremua nahitaezkoa da :values dagoenean.',
    'required_with_all'    => ':attribute eremua nahitaezkoa da :values dagoenean.',
    'required_without'     => ':attribute eremua nahitaezkoa da :values ez dagoenean.',
    'required_without_all' => ':attribute eremua nahitaezkoa da :values eremurik ez dagoenean.',
    'same'                 => ':attribute eta :other eremuek kointziditu behar dute.',
    'size'                 => [
        'numeric' => ':attribute eremuak :size izan behar du.',
        'file'    => ':attribute fitxategiak :size kilobyteko pisua izan behar du.',
        'string'  => ':attribute eremuak :size karaktere izan behar ditu.',
        'array'   => ':attribute eremuak :size elementu izan behar ditu.',
    ],
    'string'               => ':attribute eremuak karaktereak soilik izan ditzake.',
    'timezone'             => ':attribute eremuak baliozko zona bat izan behar du.',
    'unique'               => ':attribute elementua erabileran dago.',
    'uploaded'             => 'Akatsa izan da :attribute elementua igotzerakoan.',
    'url'                  => ':attribute eremuaren formatuak ez du baliozko URL batenarekin bat egiten.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
