# Laravel Euskaraz

Laravel 5erako euskarazko itzulpenak. Instalatzeko kopiatu `eu` direktorioa zure Laravel proiektuko `resources/lang` direktorioan.

Laravelen lehenetsitako itzulpen fitxategiak:

```
auth.php
passwords.php
pagination.php
validation.php
```
